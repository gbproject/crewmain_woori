package com.crewcloud.apps.crewmain.datamodel;


import com.crewcloud.apps.crewmain.CrewCloudApplication;
import com.crewcloud.apps.crewmain.util.PreferenceUtilities;

import java.util.List;

public class Login_v2_Result {
    public String SessionID;
    public String Domain;

    public String userID;
    public String FullName;
    public int Id;
    public String session;
    public String avatar;
    public int PermissionType;
    public String Password;
    public String NameCompany;
    public String MailAddress;
    public int CompanyNo;
    public String EntranceDate;
    public String BirthDate;
    public String CellPhone;
    public String CompanyPhone;
    public List<InformationCompany> informationcompany;

    public PreferenceUtilities prefs = CrewCloudApplication.getInstance().getPreferenceUtilities();

    public Login_v2_Result() {
        prefs = CrewCloudApplication.getInstance().getPreferenceUtilities();
    }
}