package com.crewcloud.apps.crewmain;

public class Constants {
    /*-------------------------URL--------------------------------*/
    public final static String ROOT_URL_ANDROID = "http://www.crewcloud.net/Android";
    public final static String VERSION = "/Version/";
    public final static String PACKGE = "/Package/";
    public final static String MAIL_NO = "MAIL_NO";
    public final static String BOX_NO = "BOX_NO";
    public final static String TITLE = "TITLE";
    public final static String REGDATE = "REGDATE";
    public final static String CREWMAIN = "CrewMain";
    public static final int ACTIVITY_HANDLER_NEXT_ACTIVITY = 1111;
    public static final int ACTIVITY_HANDLER_START_UPDATE = 1112;
    public static final String URL_CHANGE_PASS = "/UI/WebService/WebServiceCenter.asmx/UpdatePassword";
    public static final String URL_INSERT_DEVICE = "/UI/WebService/WebServiceCenter.asmx/InsertAndroidDevice";
    public static final String URL_DELETE_DEVICE = "/UI/WebService/WebServiceCenter.asmx/DeleteAndroidDevice";
}
