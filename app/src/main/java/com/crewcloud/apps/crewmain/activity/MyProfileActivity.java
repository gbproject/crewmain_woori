package com.crewcloud.apps.crewmain.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.crewcloud.apps.crewmain.R;
import com.crewcloud.apps.crewmain.CrewCloudApplication;
import com.crewcloud.apps.crewmain.util.PreferenceUtilities;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Dazone on 6/22/2017.
 */

public class MyProfileActivity extends BaseActivity {
    @Bind(R.id.activity_new_profile_iv_avatar)
    CircleImageView ivAvatar;

    @Bind(R.id.activity_new_profile_tv_name)
    TextView tvName;

    @Bind(R.id.activity_new_profile_tv_depart_position)
    TextView tvDepartPositionName;

    @Bind(R.id.activity_new_profile_tv_company_name)
    TextView tvCompanyName;

    @Bind(R.id.activity_new_profile_tv_company_id)
    TextView tvCompanyId;

    @Bind(R.id.activity_new_profile_tv_persion_id)
    TextView tvPersionId;

    @Bind(R.id.activity_new_profile_tv_email)
    TextView tvEmail;

    @Bind(R.id.activity_new_profile_tv_pass)
    TextView tvPass;

    @Bind(R.id.activity_new_profile_tv_phone)
    TextView tvPhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile_activity);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.myColor_PrimaryDark));
        }
        ButterKnife.bind(this);

        showData();
    }

    private void showData() {
        PreferenceUtilities preferenceUtilities = CrewCloudApplication.getInstance().getPreferenceUtilities();
        String companyName = preferenceUtilities.getCurrentCompanyName();
        String name = preferenceUtilities.getFullName();
        String email = preferenceUtilities.getEmail();
        String pass = preferenceUtilities.getPass();
        String avatar = preferenceUtilities.getAvatar();
        tvName.setText(name);
        tvEmail.setText(email);
        tvPass.setText(pass);
        tvCompanyName.setText(companyName);
        tvCompanyId.setText(preferenceUtilities.getCurrentCompanyDomain());
        tvPersionId.setText(preferenceUtilities.getUserId());
        if (!TextUtils.isEmpty(avatar)) {
            Picasso.with(this).load(CrewCloudApplication.getInstance().getPreferenceUtilities().getCurrentServiceDomain() + avatar)
                    .placeholder(R.mipmap.avatar_default).into(ivAvatar);
        }
    }

    @Override

    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.btn_back)
    public void onClickBack() {
        finish();
    }

    @OnClick(R.id.change_pass)
    public void changePass() {
        BaseActivity.Instance.callActivity(ChangePasswordActivity.class);
    }

}

